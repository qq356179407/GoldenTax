﻿namespace GoldenTax
{
    partial class FormLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        	this.labelTitle = new System.Windows.Forms.Label();
        	this.textBoxLoginid = new System.Windows.Forms.TextBox();
        	this.textBoxPswd = new System.Windows.Forms.TextBox();
        	this.labelUser = new System.Windows.Forms.Label();
        	this.labelPass = new System.Windows.Forms.Label();
        	this.buttonLogin = new System.Windows.Forms.Button();
        	this.SuspendLayout();
        	// 
        	// labelTitle
        	// 
        	this.labelTitle.AutoSize = true;
        	this.labelTitle.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
        	this.labelTitle.Location = new System.Drawing.Point(164, 256);
        	this.labelTitle.Name = "labelTitle";
        	this.labelTitle.Size = new System.Drawing.Size(152, 17);
        	this.labelTitle.TabIndex = 0;
        	this.labelTitle.Text = "上海鹰峰电子科技有限公司";
        	// 
        	// textBoxLoginid
        	// 
        	this.textBoxLoginid.Location = new System.Drawing.Point(277, 100);
        	this.textBoxLoginid.Name = "textBoxLoginid";
        	this.textBoxLoginid.Size = new System.Drawing.Size(144, 21);
        	this.textBoxLoginid.TabIndex = 1;
        	this.textBoxLoginid.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxLoginid_KeyDown);
        	// 
        	// textBoxPswd
        	// 
        	this.textBoxPswd.Location = new System.Drawing.Point(277, 148);
        	this.textBoxPswd.Name = "textBoxPswd";
        	this.textBoxPswd.PasswordChar = '*';
        	this.textBoxPswd.Size = new System.Drawing.Size(144, 21);
        	this.textBoxPswd.TabIndex = 2;
        	this.textBoxPswd.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxPswd_KeyDown);
        	// 
        	// labelUser
        	// 
        	this.labelUser.AutoSize = true;
        	this.labelUser.Location = new System.Drawing.Point(275, 85);
        	this.labelUser.Name = "labelUser";
        	this.labelUser.Size = new System.Drawing.Size(41, 12);
        	this.labelUser.TabIndex = 3;
        	this.labelUser.Text = "用户名";
        	// 
        	// labelPass
        	// 
        	this.labelPass.AutoSize = true;
        	this.labelPass.Location = new System.Drawing.Point(275, 133);
        	this.labelPass.Name = "labelPass";
        	this.labelPass.Size = new System.Drawing.Size(29, 12);
        	this.labelPass.TabIndex = 4;
        	this.labelPass.Text = "密码";
        	// 
        	// buttonLogin
        	// 
        	this.buttonLogin.Location = new System.Drawing.Point(277, 185);
        	this.buttonLogin.Name = "buttonLogin";
        	this.buttonLogin.Size = new System.Drawing.Size(75, 23);
        	this.buttonLogin.TabIndex = 5;
        	this.buttonLogin.Text = "登录";
        	this.buttonLogin.UseVisualStyleBackColor = true;
        	this.buttonLogin.Click += new System.EventHandler(this.buttonLogin_Click);
        	// 
        	// FormLogin
        	// 
        	this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
        	this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        	this.BackgroundImage = global::GoldenTax.Properties.Resources.taxes;
        	this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
        	this.ClientSize = new System.Drawing.Size(464, 282);
        	this.Controls.Add(this.buttonLogin);
        	this.Controls.Add(this.labelPass);
        	this.Controls.Add(this.labelUser);
        	this.Controls.Add(this.textBoxPswd);
        	this.Controls.Add(this.textBoxLoginid);
        	this.Controls.Add(this.labelTitle);
        	this.DoubleBuffered = true;
        	this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
        	this.Name = "FormLogin";
        	this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
        	this.Text = "开票处理接口登录 87627850@qq.com";
        	this.ResumeLayout(false);
        	this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.TextBox textBoxLoginid;
        private System.Windows.Forms.TextBox textBoxPswd;
        private System.Windows.Forms.Label labelUser;
        private System.Windows.Forms.Label labelPass;
        private System.Windows.Forms.Button buttonLogin;
    }
}